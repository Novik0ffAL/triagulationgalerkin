package Triangulation;

public class Triangle {
    //номера узлов  сетки
    private Node a, b, c;

    // конструктор для треугольника
    Triangle(Node a, Node b, Node c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * Функция проверяет одинаковые треугольники или нет
     *
     * @param b второй треугольник
     * @return true в случае если треугольники одинаковые по трем вершинам
     */
    public boolean equals(Triangle b) {
        if (this.a.getIndex() == b.a.getIndex() && this.b.getIndex() == b.b.getIndex() && this.c.getIndex() == c.getIndex()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Функция подсчитывет площадь треугольника
     *
     * @return площадь треугольника
     */
    public double square() {
        // сороный треугольника
        double ab = distance(a, b);
        double bc = distance(b, c);
        double ac = distance(a, c);
        // полупериметр
        double p = (ac + bc + ab) / 2;

        return Math.sqrt(p * (p - ac) * (p - ab) * (p - bc));
    }

    /**
     * Функуия расчитывает длину стороны треугольника
     * @param a узел треугольника
     * @param b узел треугольника
     * @return длину
     */
    private double distance(Node a, Node b) {
        return Math.sqrt((a.getX() - b.getX()) * a.getX() - b.getX() + (a.getY() - b.getY()) * (a.getY() - b.getY()));
    }
}
