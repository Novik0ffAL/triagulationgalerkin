package Triangulation;

import java.util.ArrayList;

public class Node {
    // координаты x, y и заряд q
    private double x, y, q;
    //уникальный индекс
    private int index;
    // атомы соседи
    private ArrayList<Integer> neighbour = new ArrayList<>();

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getQ() {
        return q;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setQ(double q) {
        this.q = q;
    }

    public int getNeighbour(int index) {
        return neighbour.get(index);
    }

    public int coutNeighbour() {
        return neighbour.size();
    }

    public void setNeighbour(int index, int value) {
        neighbour.set(index, value);
    }

    public void clearNeighbour() {
        neighbour.clear();
    }

    public boolean equals(Node b) {
        if (this.x == b.x && this.y == y) {
            return true;
        } else {
            return false;
        }
    }

    public int getIndex() {
        return this.index;
    }
}
